## Blackbox - m31
This project is a trading blackbox (Andromeda version).  It contains modules for watching stocks and different securities, an Alpha model,
Risk model, and Transaction Cost Model.  A workflow engine will be used to manage the order flow and a Rete-based
rules engine to handle logic.


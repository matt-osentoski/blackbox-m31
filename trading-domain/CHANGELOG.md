#Changelog
All notable changes to this project will be documented in this file.

The format is a subset of [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project uses parts of [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

##1.0.0-SNAPSHOT  (TODO - Remove once development settles)
### Added
- First release




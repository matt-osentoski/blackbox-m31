package com.garagebandhedgefund.blackbox.domain;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@Table(name="equity_positions")
public class EquityPosition {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    @Column(name = "id")
    private int id;

    /**
     * Unique identifier provided by the broker
     */
    @Column(name = "broker_identifier")
    private String brokerIdentifier;

    /**
     * Number of shares
     */
    @Column(name = "quantity")
    private int quantity;

    /**
     * Stock symbol
     */
    @Column(name = "symbol")
    private String symbol;

    /**
     * The price the security was purchased
     */
    @Column(name = "purchase_price")
    private double purchasePrice;

    @Column(name="date_of_purchase")
    private Date dateOfPurchase;

    @Column(name="date_position_closed")
    private Date datePositionClosed;

    @Column(name = "position_type")
    @Enumerated(EnumType.STRING)
    private PositionType positionType;

    @Column(name ="position_state")
    @Enumerated(EnumType.STRING)
    private PositionState positionState;

    public enum PositionType {
        LONG,
        SHORT
    }

    public enum PositionState {
        OPEN,
        CLOSED
    }
}

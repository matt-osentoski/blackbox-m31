package com.garagebandhedgefund.blackbox.domain;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@Table(name="equity_trade")
public class EquityTrade {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    @Column(name = "id")
    private int id;

    /**
     * Unique identifier provided by the broker
     */
    @Column(name = "broker_identifier")
    private String brokerIdentifier;

    /**
     * Number of shares
     */
    @Column(name = "quantity")
    private int quantity;

    @Column(name = "created_at")
    private Date createdAt;

    /**
     * The date/time an execution report was received by your broker for a filled/partially filled order
     */
    @Column(name = "reported_date")
    private Date reportedDate;

    @Column(name = "tradeAction")
    @Enumerated(EnumType.STRING)
    private TradeAction tradeAction;

    @Column(name = "tradeStatus")
    @Enumerated(EnumType.STRING)
    private TradeStatus tradeStatus;

    public enum TradeAction {
        BUY,
        SELL
    }

    public enum TradeStatus {
        FILLED,
        EXPIRED,
        CANCELLED,
        REJECTED
    }
}

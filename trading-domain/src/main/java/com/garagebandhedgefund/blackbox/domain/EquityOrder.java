package com.garagebandhedgefund.blackbox.domain;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;

/**
 * EquityOrder is the instruction to buy/sell an equity based on certain criteria. (ex: long/short, at the market price, etc.)
 * Once an order has been executed, a EquityPosition object is created with details about the security that was traded.
 * <br/><br/>
 * To enter a position, the workflow is:<br/>
 * Order -> Trade -> Position <br/>
 * To exit a position, the worflow is: <br/>
 *Position -> Order -> Trade
 */
@Data
@Entity
@Table(name="equity_orders")
public class EquityOrder {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    @Column(name = "id")
    private int id;

    /**
     * Unique identifier provided by the broker
     */
    @Column(name = "broker_identifier")
    private String brokerIdentifier;

    /**
     * Number of shares
     */
    @Column(name = "quantity")
    private int quantity;

    /**
     * Stock symbol
     */
    @Column(name = "symbol")
    private String symbol;

    /**
     * Applies to Limit orders
     */
    @Column(name = "limit_price")
    private double limitPrice;

    /**
     * Applies to Stop orders
     */
    @Column(name = "activation_price")
    private double activationPrice;

    @Column(name = "order_action")
    @Enumerated(EnumType.STRING)
    private OrderAction orderAction;

    @Column(name = "order_type")
    @Enumerated(EnumType.STRING)
    private OrderType orderType;

    @Column(name = "time_in_force")
    @Enumerated(EnumType.STRING)
    private TimeInForce timeInForce;

    @Column(name = "time_in_force_date")
    private Date timeInForceDate;

    @Column(name = "created_at")
    private Date createdAt;

    public enum OrderAction {
        BUY,
        SELL,
        BUY_TO_COVER,
        SELL_SHORT
    }

    public enum OrderType {
        LIMIT,
        MARKET,
        STOP_MARKET,
        STOP_LIMIT,
        TRAILING_STOP_PERCENTAGE,
        TRAILING_STOP_DOLLAR
    }

    public enum TimeInForce {
        DAY,
        DAY_EXTENDED_HOURS,
        GTC,
        GTC_EXTENDED_HOURS
    }
}
